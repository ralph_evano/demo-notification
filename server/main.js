import publications from './publications';
import methods from './methods';
import test from './configs/test';

test();
publications();
methods();
