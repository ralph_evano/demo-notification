import {useDeps, composeAll, composeWithTracker, compose} from 'mantra-core';

import Testing from '../components/testing.jsx';

export const composer = ({context}, onData) => {
  const {Meteor, Collections} = context();

  onData(null, {});
};

export const depsMapper = (context, actions) => ({
  context: () => context,
  test:actions.testing.test,
});

export default composeAll(
  composeWithTracker(composer),
  useDeps(depsMapper)
)(Testing);
