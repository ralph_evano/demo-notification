import React from 'react';

class Testing extends React.Component {
  constructor(props) {
    super(props);
  }

  test(){
    this.props.test();
    console.log("ssadasd");
    Push.send({
      from: 'push',
      title: 'Hello World',
      text: 'This notification has been sent from the SERVER',
      badge: 1,
      payload: {
        title: 'Hello World',
        historyId: "23232"
      },
      query: {}
    });
  }

  event(){
    // Internal events
    Push.addListener('token', function(token) {
      // Token is { apn: 'xxxx' } or { gcm: 'xxxx' }
    });

    Push.addListener('error', function(err) {
      if (error.type == 'apn.cordova') {
        console.log(err.error);
      }
    });

    Push.addListener('register', function(evt) {
      // Platform specific event - not really used
    });

    Push.addListener('alert', function(notification) {
      // Called when message got a message in forground
    });

    Push.addListener('sound', function(notification) {
      // Called when message got a sound
    });

    Push.addListener('badge', function(notification) {
      // Called when message got a badge
    });

    Push.addListener('startup', function(notification) {
      // Called when message recieved on startup (cold+warm)
    });

    Push.addListener('message', function(notification) {
      // Called on every message
    });
  }

  render() {
    return (
      <div>
        {this.event()}
        <button onClick={this.test.bind(this)}>Testing</button>
      </div>
    );
  }
}

export default Testing;
